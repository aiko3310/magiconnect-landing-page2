import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import IntersectionVisible from 'react-intersection-visible';

import { bannerLogo, bannerPic } from 'assets/img';

import { StyledCotainer, StlyedLogo } from './StyledBanner';
const Banner = () => {
  const { t, i18n } = useTranslation('translation');
  const [scrollToThisElement, setScrollToThisElement] = useState(false);
  const language = i18n.languages[0];
  const onShow = (e) => {
    if (e && !scrollToThisElement) {
      setScrollToThisElement(true);
    }
  };
  return (
    <IntersectionVisible onShow={onShow} options={{ rootMargin: '-20%' }}>
      <StyledCotainer scrollToThisElement={scrollToThisElement}>
        <StlyedLogo language={language} scrollToThisElement={scrollToThisElement}>
          <img src={bannerLogo} alt='' />
          <h3>{t('banner.title')}</h3>
          <h4>
            {t('banner.introduction')}
            <img src={bannerPic} alt='' />
          </h4>
        </StlyedLogo>
        <img src={bannerPic} alt='' />
      </StyledCotainer>
    </IntersectionVisible>
  );
};

export default Banner;
