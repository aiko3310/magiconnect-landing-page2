import { Img4312DD4G, IR20D } from 'assets/img';
const ApplicableModelArr = [
  {
    imgSrc: Img4312DD4G,
    title: 'IMG-4312D+-D4G',
    introductionTitle: 'modelOne',
    introduction: ['one', 'two', 'three', 'four', 'five', 'six'],
    href: process.env.REACT_APP_IMG4312DD4G_HREF,
  },
  {
    imgSrc: IR20D,
    title: 'IR-20D',
    introductionTitle: 'modelTwo',
    introduction: ['one', 'two', 'three', 'four', 'five'],
    href: process.env.REACT_APP_IR20D_HREF,
  },
];

export default ApplicableModelArr;
