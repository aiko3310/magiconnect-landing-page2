import React from 'react';
import { StyledTitle } from './StyledStyledTitle';
const title = ({ children, ...props }) => {
  return <StyledTitle {...props}>{children}</StyledTitle>;
};

export default title;
