import React, { useState, useContext } from 'react';
import { ContextStore } from 'container/App/ContextStore';
import { useTranslation } from 'react-i18next';
import languageOptions, { getLang } from './LangOptions';
import { GlobeIcon, Triangle } from 'assets/img';

import { StlyedContainer, StyledTitle, StyledUl, StyledTriangle } from './StyledLangSelector';

const LangSelector = ({ setSidebarOpen }) => {
  const { i18n } = useTranslation('translation');
  const { state } = useContext(ContextStore);
  const { phoneSize } = state;
  const [open, setOpen] = useState(false);
  const [lang, setLang] = useState(getLang(i18n.languages[0]));
  const handleToggle = () => {
    setOpen((prevState) => !prevState);
  };
  const handleMouseEnter = () => {
    if (!phoneSize) {
      setOpen(true);
    }
  };
  const handleMouseLeave = () => {
    if (!phoneSize) {
      setOpen(false);
    }
  };
  const handleLang = (language) => {
    i18n.changeLanguage(language.value);
    setLang(language);
    setOpen(false);
    setSidebarOpen(false);
  };
  const renderLang = languageOptions
    .filter((language) => (phoneSize ? true : language.value !== lang.value))
    .map((language) => (
      <li key={language.value} onClick={() => handleLang(language)} onMouseEnter={handleMouseEnter}>
        {language.label}
      </li>
    ));
  return (
    <StlyedContainer onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
      <StyledTitle onClick={handleToggle} open={open}>
        <GlobeIcon />
        <div>
          <span>{phoneSize ? 'Language' : lang.label}</span>
          <StyledTriangle open={open}>
            <Triangle />
          </StyledTriangle>
        </div>
      </StyledTitle>
      <StyledUl open={open}>{renderLang}</StyledUl>
    </StlyedContainer>
  );
};

export default LangSelector;
