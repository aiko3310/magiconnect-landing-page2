import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import StyledTitle from 'components/StyledTitle';
import { twelve, zero, freeIcon, launchIcon } from 'assets/img';
import IntersectionVisible from 'react-intersection-visible';

import {
  StyledContainer,
  StyledIntroduction,
  StyledPricingContainer,
  StyledPricingBox,
  StyledPricingTitle,
  StyledPricingDivider,
  StyledPricingContent,
  StyledPricingImg,
} from './StyledPricing';
const Pricing = () => {
  const { t, i18n } = useTranslation('translation');
  const language = i18n.languages[0];
  const [scrollToThisElement, setScrollToThisElement] = useState(false);
  const onShow = (e) => {
    if (e && !scrollToThisElement) {
      setScrollToThisElement(true);
    }
  };
  return (
    <IntersectionVisible onShow={onShow} options={{ rootMargin: '-20%' }}>
      <StyledContainer name='pricing'>
        <StyledTitle scrollToThisElement={scrollToThisElement}>{t('pricing.title')}</StyledTitle>
        <StyledIntroduction scrollToThisElement={scrollToThisElement}>{t('pricing.introduction')}</StyledIntroduction>
        <StyledPricingContainer>
          <StyledPricingBox scrollToThisElement={scrollToThisElement} xWay='left'>
            <StyledPricingTitle first>
              <img src={zero} alt='' />
              <p>{t('pricing.firstYear')}</p>
            </StyledPricingTitle>
            <StyledPricingDivider first />
            <StyledPricingContent first>
              <p>{t('pricing.freeContent')}</p>
              <StyledPricingImg src={freeIcon} alt='' first />
            </StyledPricingContent>
          </StyledPricingBox>
          <StyledPricingBox scrollToThisElement={scrollToThisElement} xWay='right'>
            <StyledPricingTitle>
              <img src={twelve} alt='' />
              <p>{t('pricing.secondYear')}</p>
            </StyledPricingTitle>
            <StyledPricingDivider />
            <StyledPricingContent>
              <p>{t('pricing.paymentContent')}</p>
              {language === 'en' ? <br /> : ''}
              <StyledPricingImg src={launchIcon} alt='' />
            </StyledPricingContent>
          </StyledPricingBox>
        </StyledPricingContainer>
      </StyledContainer>
    </IntersectionVisible>
  );
};

export default Pricing;
