import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';

import en from './i18n/en.json';
import tw from './i18n/tw.json';
import cn from './i18n/cn.json';
const getLang = () => {
  switch (window.navigator.language) {
    case 'en': {
      return 'en';
    }
    case 'zh-TW': {
      return 'tw';
    }
    case 'zh-CN': {
      return 'cn';
    }
    default: {
      return 'en';
    }
  }
};
const resources = {
  en: {
    translation: en,
  },
  tw: {
    translation: tw,
  },
  cn: {
    translation: cn,
  },
};
i18n
  .use(Backend)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init(
    {
      resources,
      debug: false,
      lng: getLang(), //預設語言
      fallbackLng: 'en', //如果當前切換的語言沒有對應的翻譯則使用這個語言
      react: {
        useSuspense: false,
      },
      interpolation: {
        escapeValue: false,
      },
    },
    (err) => {
      if (err) {
        console.log(err);
      }
    }
  );
export default i18n;
