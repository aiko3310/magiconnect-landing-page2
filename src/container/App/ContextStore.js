import { createContext } from 'react';
const initialState = {
  phoneSize: false,
  scrollY: window.pageYOffset,
};
const ContextStore = createContext({
  state: {
    phoneSize: false,
    scrollY: window.pageYOffset,
  },
});

export { initialState, ContextStore };
