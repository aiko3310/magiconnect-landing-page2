
# magiconnect

使用工具

- plop
- prettier
- react-i18next
- @react-hook/window-size 偵測 size
- react-scroll 導覽列 click 到指定位置
- react-intersection-visible 偵測已經滾動到 element

## 翻譯

在 src/i18n, i18n 入口點在 src/i18n.js

## 檔案介紹

## 網址設定檔

- 所有要外連的網址已包含在裡面
- .env.development => dev 環境下的設定
- .env.production => production 環境下的設定

### container

- app 彙整所有 component, 裡面會偵測是不是已經到了 phone size

### components

- 除了 langSeletor 為 i18n 選擇器, 其餘皆以區塊來分 compoent

### utility

- animation 對動畫做封裝
- rwd 尺寸設定檔
- theme 控制所有顏色
